# украл отсюда: https://airflow.apache.org/docs/apache-airflow/stable/howto/docker-compose/index.html
set -e -x

curl -LfO 'https://airflow.apache.org/docs/apache-airflow/2.9.2/docker-compose.yaml'
mkdir -p ./dags ./logs ./plugins ./config
cd config && curl -LfO 'https://raw.githubusercontent.com/apache/airflow/main/airflow/config_templates/airflow_local_settings.py' && cd -
echo -e "AIRFLOW_UID=$(id -u)" > .env
docker-compose up airflow-init

docker-compose up

# http://localhost:8080
# login: airflow, password: airflow
